import notify2 as notify
from time import sleep

sit_time = 20 * 60
stand_time = 8 * 60

notify.init("Tatsu")

sit_message = notify.Notification("Sit Down")
sit_message.timeout = 5000
stand_message = notify.Notification("Stand Up")
stand_message.timeout = 5000

while True:
    sleep(sit_time)
    stand_message.show()
    sleep(stand_time)
    sit_message.show()
